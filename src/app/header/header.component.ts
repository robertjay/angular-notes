import { Component, OnInit } from '@angular/core';
import { NoteService } from '../note-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private noteService : NoteService,
    public router : Router) {
     }

  ngOnInit() {
  }

  onReload(){
    this.noteService.reload();
  }

  onSave(){
    this.noteService.save();
  }
}
