const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


mongoose.connect("mongodb://localhost:27017");
mongoose.Promise = global.Promise;
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get('/api/notes', function(req, res){
    db.collection("notes").find({}, 
        (err, cursor) => cursor.toArray((err, docs) =>res.json(docs.map(x => x.text)) ));
});

app.post('/api/notes', function(req, res){
    var notes = req.body.map(x => {
        return {text : x };
    });
    db.dropCollection("notes", function(err, result){
        db.collection("notes").insertMany(notes, (err, docs)=> {
            console.log(err);
            res.end();
        });
    });
});

var port = 8080;
app.listen(port);
console.log("App listening on port "+port);