import { Component, HostListener } from '@angular/core';
import { NoteSavedEvent } from '../note/note.component';
import { NoteService } from '../note-service.service';

@Component({
  selector: 'app-note-console',
  templateUrl: './note-console.component.html',
  styleUrls: ['./note-console.component.css']
})
export class NoteConsoleComponent {


  get notes() : string[]{
    return this.noteService.getNotes() || [];
  }

  constructor(private noteService : NoteService){
  }

  onNoteSaved(event : NoteSavedEvent, elref){
    console.log(elref);
    this.noteService.addNote(event.noteContent);
  }

  onNoteClicked(noteIndex : number){
    this.noteService.removeNote(noteIndex);
  }

}
