import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class NoteService {

    private notes : string[];

    constructor(private httpClient : HttpClient){
    }

    public getNotes() : string[] {
        if (!this.notes){
            this.reload();
        }
        return this.notes;
    }

    public addNote(newNote : string){
        this.notes.push(newNote);
    }

    public removeNote(noteIndex : number){
        this.notes.splice(noteIndex, 1);
    }

    public save(){
        this.httpClient.post('http://localhost:8080/api/notes', this.notes)
        .subscribe(res => console.log(res));
    }

    public reload(){
        this.httpClient.get<string[]>('http://localhost:8080/api/notes')
            .subscribe(notes => {
                this.notes = notes;
            });
    }
}