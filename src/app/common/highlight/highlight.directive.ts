import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  private defaultBackgroundColor = "transparent";
  @Input('appHighlight') private highlightBackgroundColor = "yellow";

  @HostBinding('style.backgroundColor') backgroundColor = this.backgroundColor;

  @HostListener('mouseenter') onMouseEnter(){
    this.backgroundColor = this.highlightBackgroundColor;
  }

  @HostListener('mouseleave') onmouseleave(){
    this.backgroundColor = this.defaultBackgroundColor;
  }

}
