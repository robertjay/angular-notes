import { FilterListPipe } from './filter-list.pipe';

describe('FilterListPipe', () => {

  it('create an instance', () => {
    const pipe = new FilterListPipe();
    expect(pipe).toBeTruthy();
  });

  it('filter a list', () => {
    const pipe = new FilterListPipe();
    const list = [
      "value_1_A",
      "value_2_B",
      "value_2_C",
      "value_3_a"
    ]
    var result = pipe.transform(list, "_2_");
    expect(result.length).toBe(2);
    expect(result[0]).toBe("value_2_B");
    expect(result[1]).toBe("value_2_C");
  });

});
