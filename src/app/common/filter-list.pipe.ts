import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterList',
  pure: false
})
export class FilterListPipe implements PipeTransform {

  transform(list: string[], searchPattern : string): string[] {
    return list
      .filter((element : string) => element.toLocaleLowerCase()
        .includes(searchPattern.toLocaleLowerCase()));
  }

}
