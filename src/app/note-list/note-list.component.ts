import { Component, OnInit, Input, Output, QueryList,
  EventEmitter,OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {

  private selectedRowIndex : number = -1;
  private _noteList : string[];
  defaultBackgroundColor : string = "transparent";
  @Input('highlightColor') highlightBackgroundColor : string = "transparent";

  @Input('note-list') set noteList(list : string[]) {
    this._noteList = list;
  }

  filterValue : string = "";

  @Output('noteClicked') private noteClicked = new EventEmitter<{ noteIndex : number, noteContent : string }>();

  get noteList() {
    return this._noteList;
  }

  @Input('note-list-as-string') set noteListAsString(notes : string){
    this._noteList = notes.split(',');
  }

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => this.filterValue=params["search"] || ""); 
  }

  getBackgroundColor(rowIndex: number) : string {
    return this.selectedRowIndex === rowIndex ? 
    this.highlightBackgroundColor 
    : this.defaultBackgroundColor;
  }

  onMouseEnter(rowIndex: number){
    this.selectedRowIndex = rowIndex;
  }

  onMouseLeave(rowIndex: number){
    this.selectedRowIndex = -1;
  }

  onNoteClicked(rowIndex: number){
    this.noteClicked.emit({
      noteIndex : rowIndex,
      noteContent : this._noteList[rowIndex]
    });
  }

}
