import { Component, HostListener } from '@angular/core';
import { NoteSavedEvent } from './note/note.component';
import { NoteService } from './note-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
