import { Component, EventEmitter, Output,  Renderer2 } from "@angular/core";

@Component({
    selector: "app-note",
    templateUrl: "./note.component.html"
})
export class NoteComponent {
    messageText = "Premi il pulsante"
    note:string = "FIRST NOTE";
    @Output() noteSaved = new EventEmitter<NoteSavedEvent>();

    constructor(private renderer : Renderer2){
    }

    onClick(inputText){
        if (!inputText.value)
            return;

       this.noteSaved.emit({
            noteContent : inputText.value
        });
        this.renderer.setProperty(inputText, "value", "");
    }

}

export class NoteSavedEvent {
    noteContent : string;
}