import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteConsoleComponent } from './note-console.component';

describe('NoteConsoleComponent', () => {
  let component: NoteConsoleComponent;
  let fixture: ComponentFixture<NoteConsoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteConsoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteConsoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
