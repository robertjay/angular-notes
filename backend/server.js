const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var notes = [
    'Esercitarsi con Angular',
    'Ricordarsi di esercitarsi con Angular'
];

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get('/api/notes', function(req, res){
    res.json(notes);
});

app.post('/api/notes', function(req, res){
    notes = req.body;
    res.end();
});

var port = 8080;
app.listen(port);
console.log("App listening on port "+port);