import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'conditionatedUppercase'
})
export class ConditionatedUppercasePipe implements PipeTransform {

  transform(value: string, condition : boolean, ignored): string {
    console.log(ignored);
    if (condition)
      return value.toUpperCase();
    return value;
  }

}
