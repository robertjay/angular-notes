/*import { TestBed, async, inject } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NoteComponent } from './note/note.component';
import { NoteListComponent } from './note-list/note-list.component';
import { NoteService } from './note-service.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;

    TestBed.configureTestingModule({
      declarations: [
        AppComponent, HeaderComponent, NoteComponent, NoteListComponent
      ],
      imports: [ 
         HttpClientTestingModule ],
      providers: [NoteService, HttpClient]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);

  }));

  it('should create the app', async(inject([HttpClient, HttpTestingController],
     ()  => {
    const fixture = TestBed.createComponent(AppComponent);
     const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  })));

  it(`should have an header`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector("app-header")).toBeTruthy();
  });

  it(`should have a note`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector("app-note")).toBeTruthy();
  });

  it(`should have a note list`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector("app-note-list")).toBeTruthy();
  });


});
*/