import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { FormsModule } from '@angular/forms'
import {HttpClientModule} from '@angular/common/http'
import {RouterModule, Route} from '@angular/router'

import { AppComponent } from './app.component';
import { NoteComponent } from './note/note.component';
import { NoteListComponent } from './note-list/note-list.component';
import { NoteService } from './note-service.service';
import { HeaderComponent } from './header/header.component';
import { HighlightDirective } from './common/highlight/highlight.directive';
import { ConditionatedUppercasePipe } from './common/conditionated-uppercase.pipe';
import { HomeComponent } from './home/home.component';
import { NoteConsoleComponent } from './note-console/note-console.component';
import { FilterListPipe } from './common/filter-list.pipe';

const routes : Route[] = [
  { path : "home", component : HomeComponent },
  { path : "notes", component : NoteConsoleComponent },
  { path : "notes/:search", component : NoteConsoleComponent },
  { path : "", component : HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NoteComponent,
    NoteListComponent,
    HeaderComponent,
    HighlightDirective,
    ConditionatedUppercasePipe,
    HomeComponent,
    NoteConsoleComponent,
    FilterListPipe,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [NoteService],
  bootstrap: [AppComponent]
})
export class AppModule { }


